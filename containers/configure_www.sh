#!/bin/bash

# TODO
# apache => logs d'accès et d'erreur

################################################################################

################################################################################

# BSD 3-Clause License
# 
# Copyright (c) 2018-2019, Alban Vidal <alban.vidal@zordhak.fr>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
# 
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
# 
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

################################################################################

################################################################################
##########                    Define color to output:                 ########## 
################################################################################
_WHITE_="tput sgr0"
_RED_="tput setaf 1"
_GREEN_="tput setaf 2"
_ORANGE_="tput setaf 3"
################################################################################

# Path of git repository
# ../
GIT_PATH="$(realpath ${0%/*/*})"

################################################################################
#### Load path of install_conf_lxd git directoy

source $GIT_PATH/config/00_VARS

################################################################################
#### Source local conf

source $GIT_PATH/config/vars

################################################################################
#### Source « install_conf_lxd » conf

# Load Network Vars
source $GIT_PATH_install_conf_lxd/config/01_NETWORK_VARS

# Load Other vars 
# - LXD_DEPORTED_DIR
# - DEBIAN_RELEASE
source $GIT_PATH_install_conf_lxd/config/03_OTHER_VARS

################################################################################
#### Load project vars

PREFIX=$1
source $GIT_PATH/config/${PREFIX}

################################################################################

### BEGIN « www » container

echo
echo "$($_GREEN_)~~ BEGIN « www » container ~~$($_WHITE_)"

################################################################################

echo "$($_ORANGE_)Create symlinks for /etc/apache2, /etc/icinga2, /etc/icingaweb2 and /var/lib/icinga2 to /srv/lxd$($_WHITE_)"
lxc exec ${PREFIX}-www -- bash -c "
    ln -s /srv/lxd/etc/apache2          /etc/
    ln -s /srv/lxd/etc/icinga2          /etc/
    ln -s /srv/lxd/etc/icingaweb2       /etc/
    ln -s /srv/lxd/var/lib/icinga2      /var/lib/
    ln -s /srv/lxd/srv/icinga2-hosts.d  /srv/
"

################################################################################

## SSL CERTIFICATE

echo "$($_ORANGE_)Generating certificates: $FQDN$($_WHITE_)"
if $CREATE_SSL_CERTIFICATES ; then
    lxc exec $RVPRX_CONTAINER -- bash -c "certbot certonly -n --agree-tos --email $TECH_ADMIN_EMAIL --nginx -d $FQDN > /dev/null"
else
    echo "$($_GREEN_)CREATE_SSL_CERTIFICATES=false, don't create certificates, you need to setup it manually$($_WHITE_)"
fi

################################################################################

## Nginx vhost

echo "$($_ORANGE_)Nginx: Conf, Vhosts and tuning$($_WHITE_)"

# Test if /etc/nginx/RVPRX_common.conf file exist
lxc exec $RVPRX_CONTAINER -- bash -c "
    if [ ! -f /etc/nginx/RVPRX_common.conf ] ; then
        echo 'WARNING'
        echo 'No file /etc/nginx/RVPRX_common.conf are present'
        echo 'Needed for common nginx configuration'
        echo 'https://framagit.org/zorval/lxd_deploy_apps/install_conf_lxd/blob/master/templates/rvprx/etc/nginx/RVPRX_common.conf'
    fi
"

echo "$($_ORANGE_)Create vhost$($_WHITE_)"
lxc exec $RVPRX_CONTAINER -- bash -c "
cat << EOF > /etc/nginx/sites-available/rvprx-$FQDN.conf
server {
    listen      80;
    server_name $FQDN;
    return 301  https://$FQDN\$request_uri;
}

server {
    listen      443 ssl http2;
    server_name $FQDN;

    # Let's Encrypt:
    ssl_certificate     /etc/letsencrypt/live/$FQDN/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/$FQDN/privkey.pem;

    # Add common Conf:
    # See https://framagit.org/zorval/lxd_deploy_apps/install_conf_lxd/blob/master/templates/rvprx/etc/nginx/RVPRX_common.conf
    include /etc/nginx/RVPRX_common.conf;

    # LOGS
    gzip on;
    access_log /var/log/nginx/${FQDN}_access.log;
    error_log  /var/log/nginx/${FQDN}_error.log;

    location / { proxy_pass http://${WWW_IP_PRIV}/; }
}
EOF
"

echo "$($_ORANGE_)Enable vhost$($_WHITE_)"
echo
lxc exec $RVPRX_CONTAINER -- bash -c "
    ln -sf /etc/nginx/sites-available/rvprx-$FQDN.conf /etc/nginx/sites-enabled/
    if nginx -t ; then
        echo -e '\033[32mNginx conf OK, reload\033[0m'
        nginx -s reload
    else
        echo -e '\033[31m!!! ERROR !!! Nginx fail, NO realod\033[0m'
    fi
"

################################################################################

## End Nginx configuration
## Go install ; Icinga
# https://www.icinga.com/docs/icinga2/latest/doc/02-getting-started/

######### TODO

echo "$($_ORANGE_)Install Icinga and Icinga Web 2$($_WHITE_)"
lxc exec ${PREFIX}-www -- bash -c "
    DEBIAN_FRONTEND=noninteractive apt-get -y install \
        icinga2             \
        icingacli           \
        icingaweb2          \
        icinga2-ido-mysql   \
        mariadb-client      \
        monitoring-plugins  \
        apache2             \
        libapache2-mod-rpaf \
        php7.0-fpm          \
        curl                \
        vim-icinga2         \
        vim-addon-manager   \
        nagios-nrpe-plugin  \
        > /dev/null
    # TODO INSTALL
"

echo "$($_ORANGE_)apache2 listen only in Private IP$($_WHITE_)"
lxc exec ${PREFIX}-www -- bash -c "echo 'Listen ${WWW_IP_PRIV}:80' > /etc/apache2/ports.conf"

echo "$($_ORANGE_)Enable vim Syntax Highlighting$($_WHITE_)"
lxc exec ${PREFIX}-www -- bash -c "
    vim-addon-manager -w install icinga2
"

echo "$($_ORANGE_)Set icinga2.service auto restart when fail$($_WHITE_)"
lxc exec ${PREFIX}-www -- bash -c "
    mkdir -p /etc/systemd/system/icinga2.service.d/
    cat << EOF > /etc/systemd/system/icinga2.service.d/override.conf
[Service]
Restart=always
RestartSec=1
StartLimitInterval=10
StartLimitBurst=3
EOF
"

################################################################################

echo "$($_ORANGE_)Disable default Virtual Host, enable Icinga vhost and reload apache2$($_WHITE_)"
# Push vhost file
# To generate vhost example :
# icingacli setup config webserver apache
lxc file push $GIT_PATH/templates/www/etc/apache2/sites-available/icinga.conf ${PREFIX}-www/etc/apache2/sites-available/icinga.conf
lxc exec ${PREFIX}-www -- bash -c "
    sed -i                                             \
        -e 's/__FQDN__/$FQDN/'                         \
        -e 's/__TECH_ADMIN_EMAIL__/$TECH_ADMIN_EMAIL/' \
        /etc/apache2/sites-available/icinga.conf
    a2dissite 000-default       > /dev/null
    a2ensite icinga             > /dev/null
    systemctl reload apache2
"

################################################################################


# TODO
# file « /etc/icinga2/features-available/ido-mysql.conf » needed before inable ido-mysql
echo "$($_ORANGE_)Configure Icinga IDO Mysql$($_WHITE_)"
lxc file push $GIT_PATH/templates/www/etc/icinga2/features-available/ido-mysql.conf  ${PREFIX}-www/etc/icinga2/features-available/ido-mysql.conf
lxc exec ${PREFIX}-www -- bash -c "
    sed -i                                          \
        -e 's/__USER__/$ICINGA_IDO_USER/'           \
        -e 's/__PASSWORD__/$ICINGA_IDO_PASSWORD/'   \
        -e 's/__HOST__/$DB_IP_PRIV/'                \
        /etc/icinga2/features-available/ido-mysql.conf
"

################################################################################

echo "$($_ORANGE_)Enable Icinaga Features$($_WHITE_)"
# List features : « icinga2 feature list »
lxc exec ${PREFIX}-www -- bash -c "
    icinga2 feature enable command ido-mysql
    # perfdata is not enable
"

################################################################################

echo "$($_ORANGE_)Enable php7-fpm in apache2$($_WHITE_)"
lxc exec ${PREFIX}-www -- bash -c "
    a2enmod proxy_fcgi setenvif > /dev/null
    a2enconf php7.0-fpm         > /dev/null
"

echo "$($_ORANGE_)Create Icinga Web 2 token$($_WHITE_)"
lxc exec ${PREFIX}-www -- bash -c "
    icingacli setup token create|sed 's/.*: //' > /root/icinga_web2_token
"

echo "$($_ORANGE_)Set PHP 7 timezone$($_WHITE_)"
lxc exec ${PREFIX}-www -- bash -c "
    sed -i 's#;\(date.timezone =\)#\1 \"Europe/Paris\"#' /etc/php/7.0/fpm/php.ini
"

echo "$($_ORANGE_)Set transparency client ip for Icinga Web 2 and Apache$($_WHITE_)"
lxc exec ${PREFIX}-www -- bash -c "
    sed -i 's/\\(.*RPAFproxy_ips\\).*/\\1 $IP_rvprx_PRIV/' /etc/apache2/mods-available/rpaf.conf
"

echo "$($_ORANGE_)Copy Icinga IDO and Icinga Web 2 shemad in host for DB$($_WHITE_)"
lxc file pull ${PREFIX}-www/usr/share/icinga2-ido-mysql/schema/mysql.sql     /tmp/${PREFIX}_icinga2-ido-mysql_schema.sql
lxc file pull ${PREFIX}-www/usr/share/icingaweb2/etc/schema/mysql.schema.sql /tmp/${PREFIX}_icinga_web_2-mysql_schema.sql

################################################################################

# TODO : Modules à activer :
lxc exec ${PREFIX}-www -- bash -c "
    icingacli module enable monitoring
"

################################################################################

# TODO : copier l'arbre des fichiers de conf ! => EN COURS

#echo "$($_ORANGE_)Copy and configure Icinga and Icinga Web 2 configuration files$($_WHITE_)"
echo "$($_ORANGE_)Copy and configure Icinga Web 2 configuration files$($_WHITE_)"
cp -r $GIT_PATH/templates/www/etc/icingaweb2/ $LXD_DEPORTED_DIR/${PREFIX}/www/etc/
chown -R 1000000:1000000 $LXD_DEPORTED_DIR/${PREFIX}/www/etc/icingaweb2/
lxc exec ${PREFIX}-www -- bash -c "
# TODO : voir si necessaire de changer les permissions de /etc/icingaweb2/
# Possible si on veut pouvoir modifier la conf depuis le site
    chown -R www-data:www-data /etc/icingaweb2/
    find /etc/icingaweb2/ -type f -exec sed -i \
        -e 's/__DB_IP_PRIV__/$DB_IP_PRIV/' \
        -e 's/__ICINGA_IDO_USER__/$ICINGA_IDO_USER/' \
        -e 's/__ICINGA_IDO_PASSWORD__/$ICINGA_IDO_PASSWORD/' \
        -e 's/__ICINGA_WEB_2_ADMIN_USER__/$ICINGA_WEB_2_ADMIN_USER/' \
        -e 's/__ICINGA_WEB_2_ADMIN_PASSWORD__/$ICINGA_WEB_2_ADMIN_PASSWORD/' \
        -e 's/__ICINGA_WEB_2_DB_USER__/$ICINGA_WEB_2_DB_USER/' \
        -e 's/__ICINGA_WEB_2_DB_PASSWORD__/$ICINGA_WEB_2_DB_PASSWORD/' \
        {} \;
"

################################################################################

echo "$($_ORANGE_)Update administrator email alert$($_WHITE_)"
lxc exec ${PREFIX}-www -- bash -c "
    sed -i 's/\(.*email =\).*/\1 \"$ICINGA_ADMIN_EMAIL\"/' /etc/icinga2/conf.d/users.conf
"

################################################################################

#echo "$($_ORANGE_)Disable IPv4 and IPv6 in icinga host to disable ssh check localy$($_WHITE_)"
#lxc exec ${PREFIX}-www -- bash -c "
#    sed -i                                  \
#        -e 's#\(.*address =.*\)#// \1#'     \
#        -e 's#\(.*address6 =.*\)#// \1#'    \
#        /etc/icinga2/conf.d/hosts.conf
#"

echo "$($_ORANGE_)Edit localhost icinga check$($_WHITE_)"
# Disable old localhost conf file:
lxc exec ${PREFIX}-www -- bash -c "
    mv /etc/icinga2/conf.d/hosts.conf       /etc/icinga2/conf.d/hosts.conf.DEFAULT
    mv /etc/icinga2/conf.d/services.conf    /etc/icinga2/conf.d/services.conf.DEFAULT
    mv /etc/icinga2/conf.d/downtimes.conf   /etc/icinga2/conf.d/downtimes.conf.DEFAULT
"
# Push localhost conf file:
lxc file push $GIT_PATH/templates/www/etc/icinga2/conf.d/localhost.conf ${PREFIX}-www/etc/icinga2/conf.d/
lxc file push $GIT_PATH/templates/www/etc/icinga2/conf.d/services.conf ${PREFIX}-www/etc/icinga2/conf.d/

################################################################################

echo "$($_ORANGE_)Add hosts config directory in /srv/icinga2-hosts.d$($_WHITE_)"
lxc exec ${PREFIX}-www -- bash -c "
    # If is not already configured:
    if ! grep -q /srv/icinga2-hosts.d /etc/icinga2/icinga2.conf ; then
        echo '' >> /etc/icinga2/icinga2.conf
        echo '// Add *.conf files present in /srv/icinga2-hosts.d directory' >> /etc/icinga2/icinga2.conf
        echo 'include \"/srv/icinga2-hosts.d/*.conf\" ' >> /etc/icinga2/icinga2.conf
    fi
"

# TODO :
#echo "$($_ORANGE_)Disable Swap check in icinga host$($_WHITE_)"
# sed ...

# TODO
# Add check hosts files in /srv/icinga-hosts
# echo >> dans /etc/icinga2/icinga2.conf

################################################################################

## A VIRER :
#lxc exec ${PREFIX}-www -- bash -c '
#
#########################################
#
## TODO : authentication.ini ??
#    cat << EOF > /etc/icingaweb2/authentication.ini
#[icingaweb2]
#backend = "db"
#resource = "icingaweb_db"
#EOF
#
#########################################
#
#    cat << EOF > /etc/icingaweb2/config.ini
#[global]
#show_stacktraces = "1"
#config_backend = "db"
#config_resource = "icingaweb_db"
#
#[logging]
#log = "syslog"
#level = "ERROR"
#application = "icingaweb2"
#facility = "user"
#EOF
#
#########################################
#
## TODO : groups.ini ??
#    cat << EOF > /etc/icingaweb2/groups.ini
#[icingaweb2]
#backend = "db"
#resource = "icingaweb_db"
#EOF
#
#########################################
#
## TODO
## PASSWORD - IP
#
#    cat << EOF > /etc/icingaweb2/resources.ini
#[icingaweb_db]
#type = "db"
#db = "mysql"
#host = "192.168.200.80"
#port = ""
#dbname = "icingaweb2"
#username = "icingaweb2"
#password = "CHANGEME"
#charset = "UTF8"
#persistent = "0"
#use_ssl = "0"
#
#[icinga]
#type = "db"
#db = "mysql"
#host = "192.168.200.80"
#port = ""
#dbname = "icinga"
#username = "icinga"
#password = "icinga"
#charset = "UTF8"
#persistent = "0"
#use_ssl = "0"
#EOF
#
#########################################
#
#    cat << EOF > roles.ini
#[Administrators]
#users = "icingaweb2"
#permissions = "*"
#groups = "Administrators"
#EOF
#'

## TODO : CONFIGURER LES DROITS !!!
## www-data sur /etc/icingaweb2


## FIN

################################################################################
