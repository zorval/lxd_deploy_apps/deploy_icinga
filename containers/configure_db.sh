#!/bin/bash

# TODO
# Ajout de répertoire partagé pour dump (VOIR REP DANS SCRIPT DE DUMP
#
# mariadb ne doit écouter que sur l'interface de backend => fait ?
# 
# CREATION DE LA BASE

################################################################################

# BSD 3-Clause License
# 
# Copyright (c) 2018, Alban Vidal <alban.vidal@zordhak.fr>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
# 
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
# 
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

################################################################################

################################################################################
##########                    Define color to output:                 ########## 
################################################################################
_WHITE_="tput sgr0"
_RED_="tput setaf 1"
_GREEN_="tput setaf 2"
_ORANGE_="tput setaf 3"
################################################################################

# Path of git repository
# ../
GIT_PATH="$(realpath ${0%/*/*})"

# Load Vars
source $GIT_PATH/config/vars

# Load project vars
PREFIX=$1
source $GIT_PATH/config/${PREFIX}

################################################################################

# HASH password stored in database for administrator user
HASH_ICINGA_WEB_2_ADMIN_PASSWORD=$(openssl passwd -1 "$ICINGA_WEB_2_ADMIN_PASSWORD")

################################################################################

### DB container

echo
echo "$($_GREEN_)~~ BEGIN « db » container ~~$($_WHITE_)"

################################################################################

echo "$($_ORANGE_)Create symlinks for /var/lib/mysql to /srv/lxd$($_WHITE_)"
lxc exec ${PREFIX}-db -- bash -c "
    ln -s /srv/lxd/var/lib/mysql    /var/lib/
"

################################################################################

echo "$($_ORANGE_)Install MariaDB Server$($_WHITE_)"
lxc exec ${PREFIX}-db -- bash -c "
    DEBIAN_FRONTEND=noninteractive apt-get -y install mariadb-server > /dev/null
"

################################################################################

echo "$($_ORANGE_)Secure MariaDB (like mysql_secure_installation)$($_WHITE_)"
lxc file push --mode 500 $GIT_PATH/templates/mariadb/tmp/mariadb_secure_installation ${PREFIX}-db/tmp/lxd_mariadb_secure_installation
lxc exec ${PREFIX}-db -- bash -c "
    /tmp/lxd_mariadb_secure_installation
"

################################################################################

echo "$($_ORANGE_)Change MariaDB Bind Address$($_WHITE_)"
lxc exec ${PREFIX}-db -- bash -c "
    sed -i 's/bind-address.*/bind-address = $DB_IP_PRIV/' /etc/mysql/mariadb.conf.d/50-server.cnf
"

echo "$($_ORANGE_)Restart MariaDB$($_WHITE_)"
lxc exec ${PREFIX}-db -- bash -c "
    systemctl restart mariadb
"

################################################################################

# Create Icinga databases and user

## Icinga IDO
echo "$($_ORANGE_)Create Icinga databases and user$($_WHITE_)"
cat << EOF > /tmp/lxd_icinga_create_db_and_user
#!/bin/bash
mysql <<< "
    CREATE DATABASE icinga;
    GRANT SELECT, INSERT, UPDATE, DELETE, DROP, CREATE VIEW, INDEX, EXECUTE
    ON icinga.* 
    TO '$ICINGA_IDO_USER'@'$WWW_IP_PRIV' 
    IDENTIFIED BY '$ICINGA_IDO_PASSWORD';
    FLUSH PRIVILEGES;
"
EOF

lxc file push --mode 500 /tmp/lxd_icinga_create_db_and_user ${PREFIX}-db/tmp/lxd_icinga_create_db_and_user
lxc exec ${PREFIX}-db -- bash -c "
    /tmp/lxd_icinga_create_db_and_user
"

# Copy and import Icinga IDO schema
echo "$($_ORANGE_)Copy and import Icinga IDO schema$($_WHITE_)"
lxc file push /tmp/${PREFIX}_icinga2-ido-mysql_schema.sql ${PREFIX}-db/root/icinga2-ido-mysql_schema.sql
lxc exec ${PREFIX}-db -- bash -c "
    mysql icinga < /root/icinga2-ido-mysql_schema.sql
"

## Icinga Web 2
echo "$($_ORANGE_)Create Icinga Web 2 databases and user$($_WHITE_)"
cat << EOF > /tmp/lxd_icingaWeb2_create_db_and_user
#!/bin/bash
mysql <<< "
    CREATE DATABASE icingaweb2;
    GRANT ALL 
    ON icingaweb2.* 
    TO '$ICINGA_WEB_2_DB_USER'@'$WWW_IP_PRIV'
    IDENTIFIED BY '$ICINGA_WEB_2_DB_PASSWORD';
    FLUSH PRIVILEGES;
"
EOF

lxc file push --mode 500 /tmp/lxd_icingaWeb2_create_db_and_user ${PREFIX}-db/tmp/lxd_icingaWeb2_create_db_and_user 
lxc exec ${PREFIX}-db -- bash -c "
    /tmp/lxd_icingaWeb2_create_db_and_user
"

# Copy and import Icinga Web 2 schama
echo "$($_ORANGE_)Copy and import Icinga Web 2 schama$($_WHITE_)"
lxc file push /tmp/${PREFIX}_icinga_web_2-mysql_schema.sql ${PREFIX}-db/root/icinga_web_2-mysql_schema.sql
lxc exec ${PREFIX}-db -- bash -c "
    mysql icingaweb2 < /root/icinga_web_2-mysql_schema.sql
"

echo "$($_ORANGE_)Set password for user icingaadmin for Icinga Web 2$($_WHITE_)"
lxc exec ${PREFIX}-db -- bash -c "
    mysql icingaweb2 -Bse '
        INSERT INTO icingaweb_user
            (name, active, password_hash)
            VALUES (\"$ICINGA_WEB_2_ADMIN_USER\", 1, \"$HASH_ICINGA_WEB_2_ADMIN_PASSWORD\");
    '
"

################################################################################

# Copy MySQL dump script
echo "$($_ORANGE_)Copy MySQL dump script$($_WHITE_)"
lxc file push $GIT_PATH/templates/mariadb/mysql-auto-dump ${PREFIX}-db/usr/local/bin/

## FIN

################################################################################
