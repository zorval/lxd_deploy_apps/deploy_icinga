Deploy Icinga
=============

**Deploy icinga with lxd**

----------------------------------------

Needed:
+ LXD host already configured, see [install_conf_lxd](https://framagit.org/zorval/lxd_deploy_apps/install_conf_lxd) for more information

----------------------------------------

## Create configuration file for icinga and start deploy

+ Copy `config/example_icinga` to `config/icinga`
+ Edit values in `config/icinga` file
+ Edit usernames and passwords in `config/icinga` file
+ Start deploy by execute: `./deploy.sh icinga`

----------------------------------------

## Additional check examples

In <project-name>-www container, you can put your personnal check in `/srv/icinga2-hosts.d` directory.

You can find checks example in the [examples](./examples) directory.

----------------------------------------

## Links and documentations

+ [Icinga official - Documentation](https://www.icinga.com/docs)
+ [Icinga official - Getting started](https://www.icinga.com/docs/icinga2/latest/doc/02-getting-started)
+ [IcingaWeb official - Installation](https://www.icinga.com/docs/icingaweb2/latest/doc/02-Installation/)
